#!/bin/bash

# On installe seulement les dependances pour Docker 
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Installe git ( les images php ne l'ont pas par defaut) qui est requis par composer
apt-get update -yqq
apt-get install git -yqq

# Installe phpunit pour nos test
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
chmod +x /usr/local/bin/phpunit

# Installe de driver msql , on peut y mettre toutes les dependances que l'ont veut 
docker-php-ext-install pdo_mysql